import React from 'react'
import {connect} from 'react-redux'
import {bindActionCreators} from 'redux'

import {getMusicas, receberAlteracao, alterarPosicao} from '../../actions'
import logotipo from '../../assets/logo.png'
import {ArrowUpward, ArrowDownward} from '@material-ui/icons'
import './styles.css'

import {ioURL} from '../../services/api'
import socket from 'socket.io-client'

class Votacao extends React.Component {

    componentDidMount() {
        this.iniciarIO()
        this.props.getMusicas()
    }

    iniciarIO = () => {
        const io = socket(ioURL)

        io.on('scoreMusica', data => {
            this.props.receberAlteracao(data)
        })
    }

    handleDiminuir = async(i) => {
        i.score = i.score -1;
        this.props.alterarPosicao(i)
    }

    handleAumentar = async(i) => {
        i.score = i.score +1;
        this.props.alterarPosicao(i)
    }

    render() {
        const {carregando, atualizando, registros, error} = this.props.musicas

        return (
            <div id="box-container">
                <header>
                    <img src={logotipo} alt="" />
                    <h1>Musicas</h1>
                </header>

                {atualizando && <div>Aguarde atualizando...</div>}

                {carregando && <div>Aguarde carregando...</div>}

                <ul>
                    {!carregando && registros.length > 0 && registros.map((item, i) => (
                        <li key={i}>
                            <div>
                                <b>{item.score}</b>
                            </div>

                            <div flex="true" className="item">
                                <span className="titulo">{item.titulo}</span> <br />
                                <span>Autor: {item._autor.nome}</span> <br />
                                <span>Album: {item.album}</span>
                            </div>

                            <span>
                                <button onClick={() => this.handleDiminuir(item)}>
                                    <ArrowDownward />
                                </button>

                                <button onClick={() => this.handleAumentar(item)}>
                                    <ArrowUpward />
                                </button>
                            </span>
                        </li>
                    ))}
                </ul>

            </div>
        )
    }
}

const mapStateToProps = state => ({
    musicas: state.musicasReducer
})

const mapDispatchToProps = dispatch => bindActionCreators({receberAlteracao, alterarPosicao, getMusicas}, dispatch)

export default connect(mapStateToProps, mapDispatchToProps)(Votacao)