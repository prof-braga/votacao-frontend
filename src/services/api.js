import axios from "axios";

// export const ioURL = 'http://localhost:4000'
// export const ioURL = "https://votacao-backend.herokuapp.com";
export const ioURL = process.env.REACT_APP_API_URL;

export const api = axios.create({
  baseURL: ioURL + "/api"
});
