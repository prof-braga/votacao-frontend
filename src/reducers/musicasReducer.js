import {MUSICAS_REQUEST, MUSICAS_SUCCESS, MUSICAS_FAILURE, MUSICAS_SCORE_REQUEST, MUSICAS_SCORE_SUCCESS, MUSICAS_SCORE_FAILURE, MUSICAS_REFRESH_REQUEST, MUSICAS_REFRESH_SUCCESS} from '../actions/actionsTypes'

const initialState = {
    registros: [],
    atualizando: false,
    item: {},
    carregando: false,
    error: false,
    msgError: ''
}

export const musicasReducer = (state = initialState, action) => {
    switch(action.type) {
        case MUSICAS_REQUEST:
            return {
                ...state,
                carregando: true,
                registros: [],
                msgError: '',
                error: false
            }

        case MUSICAS_SUCCESS:
            return {
                ...state,
                carregando: false,
                registros: ordenar(action.registros),
            }

        case MUSICAS_FAILURE:
            return {
                ...state,
                carregando: false,
                msgError: action.error,
                error: true
            }

        case MUSICAS_SCORE_REQUEST:
            return  {
                ...state,
                atualizando: true,
                msgError: '',
                error: false
            }

        case MUSICAS_SCORE_SUCCESS: 
            let novosRegistros = [...state.registros.filter((regAtual) => { return regAtual._id !== action.item._id}), action.item]

            return {
                ...state,
                registros: ordenar(novosRegistros),
                atualizando: false
            }

        case MUSICAS_SCORE_FAILURE:
            return {
                ...state,
                item: {},
                atualizando: false,
                msgError: action.error,
                error: true
            }

        case MUSICAS_REFRESH_REQUEST:
            return {
                ...state,
                atualizando: true
            }

        case MUSICAS_REFRESH_SUCCESS:
            let refreshRegistros = [...state.registros.filter((regAtual) => { return regAtual._id !== action.item._id}), action.item]

            return {
                ...state,
                atualizando: false,
                registros: ordenar(refreshRegistros)
            }

        default:
            return state;
    }
}

const ordenar = (colDados) => {
    return colDados.sort(function(item1, item2) {
        return item2['score'] - item1['score']
    })
}
