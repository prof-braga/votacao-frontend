import {MUSICAS_REQUEST, MUSICAS_SUCCESS, MUSICAS_FAILURE, MUSICAS_SCORE_REQUEST, MUSICAS_SCORE_SUCCESS, MUSICAS_SCORE_FAILURE, MUSICAS_REFRESH_REQUEST, MUSICAS_REFRESH_SUCCESS} from './actionsTypes'

import {api} from '../services/api'

export function getMusicas() {
    return function action(dispatch) {
        dispatch({type: MUSICAS_REQUEST})

        api.get('/musica').then((res) => {
            dispatch({type: MUSICAS_SUCCESS, registros: res.data})
        }).catch((error) => {
            dispatch({type: MUSICAS_FAILURE, error})
        })
    }
}

export function alterarPosicao(item) {
    return function action(dispatch) {
        dispatch({type: MUSICAS_SCORE_REQUEST})

        api.put('/musica/score/' + item._id, item).then((res) => {
            dispatch({type: MUSICAS_SCORE_SUCCESS, item: res.data})
        }).catch((error) => {
            dispatch({type: MUSICAS_SCORE_FAILURE, error})
        })
    }
}

export function receberAlteracao(item) {
    return function action(dispatch) {
        dispatch({type: MUSICAS_REFRESH_REQUEST})

        setTimeout(() => {
            dispatch({type: MUSICAS_REFRESH_SUCCESS, item})
        }, 1500)
    }
}